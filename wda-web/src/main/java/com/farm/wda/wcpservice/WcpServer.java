package com.farm.wda.wcpservice;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.farm.wcp.api.WcpAppInter;
import com.farm.wda.Beanfactory;
import com.farm.wda.domain.DocTask;
import com.farm.wda.util.AppConfig;

public class WcpServer {
	/**
	 * 记录当前已经在wcp中做过索引的附件,超过100的队列长度就被清空
	 */
	private static Set<String> overIndexKeys = new HashSet<String>();
	private static final Logger log = Logger.getLogger(WcpServer.class);
	private WcpAppInter wcpApp = null;
	private static WcpServer obj = null;

	private WcpServer() {
	}

	public static WcpServer getInstance() {
		if (obj == null) {
			obj = new WcpServer();
		}
		try {
			if (obj.wcpApp == null && AppConfig.getString("config.wcp.rmi.able").toUpperCase().equals("TRUE")) {
				obj.wcpApp = (WcpAppInter) Naming.lookup(AppConfig.getString("config.wcp.rmi.url"));
			}
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			log.error("WCP RMI ERROR！" + e.getMessage(), e);
			obj.wcpApp = null;
			return obj;
		}
		return obj;
	}

	/**
	 * 执行wcp的附件索引接口
	 * 
	 * @param task
	 */
	public void runWcpFileIndex(DocTask task) {
		if (wcpApp == null) {
			log.info("WCP RMI inter is not used!");
			return;
		}
		try {
			String text = Beanfactory.getWdaAppImpl().getText(task.getKey());
			if (text != null) {
				if (overIndexKeys.size() > 100) {
					// 超过100的队列长度就被清空
					overIndexKeys.clear();
				}
				// 是否每次生成预览文件都重做索引
				boolean isMustIndex = AppConfig.getString("config.index.every.convert").toUpperCase().equals("TRUE");
				if (overIndexKeys.contains(task.getKey()) && !isMustIndex) {
					// 已经做过索引
				} else {
					// 未做过索引
					log.debug("WDA调用WCP接口实现-----附件索引,text:" + text.length());
					wcpApp.runLuceneIndex(task.getKey(), task.getAuthid(), text);
					overIndexKeys.add(task.getKey());
				}
			}
		} catch (Exception e) {
			log.error("wcp索引任务回调失败：", e);
		}
	}

	/**
	 * 预览附件转换开始事件(通知wcp任務狀態)
	 */
	public void FileConvertStartingEvent(DocTask task) {
		if (wcpApp == null) {
			log.info("WCP RMI inter is not used!");
			return;
		}
//		try {
//			wcpApp.FileConvertStartingEvent(task.getKey(), task.getAuthid(),
//					AppConfig.getString("config.wcp.rmi.secret"));
//		} catch (RemoteException e) {
//			log.error(e.getMessage(), e);
//		}
	}

	/**
	 * 预览附件转换成功事件(通知wcp任務狀態)
	 */
	public void FileConvertSuccessEvent(DocTask task) {
		if (wcpApp == null) {
			log.info("WCP RMI inter is not used!");
			return;
		}
		// try {
		// wcpApp.FileConvertSuccessEvent(task.getKey(),
		// AppConfig.getString("config.wcp.rmi.secret"));
		// } catch (RemoteException e) {
		// log.error(e.getMessage(), e);
		// }
	}

	/**
	 * 预览附件转换失败事件(通知wcp任務狀態)
	 */
	public void FileConvertErrorEvent(DocTask task, String message) {
		if (wcpApp == null) {
			log.info("WCP RMI inter is not used!");
			return;
		}
		// try {
		// //wcpApp.FileConvertErrorEvent(task.getKey(), message,
		// AppConfig.getString("config.wcp.rmi.secret"));
		// } catch (RemoteException e) {
		// log.error(e.getMessage(), e);
		// }
	}
}
